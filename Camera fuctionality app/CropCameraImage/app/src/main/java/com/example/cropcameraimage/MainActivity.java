package com.example.cropcameraimage;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.util.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {
    ImageView imageView;
    Button button;
    TextView textView;
    static final int CAMERA_ACTION_PICK_REQUEST_CODE = 1;
    String currentPhotoPath = "";
    Context context;
    Bitmap imageBitmap;

    ActivityResultLauncher<Intent> activityResultLauncher;
    static final int REQUEST_IMAGE_CAPTURE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.image);
        button = findViewById(R.id.btn);
        textView = findViewById(R.id.textView);

        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file = null;
                try {
                    file = getImageFile();//get absolute path name
                } catch (IOException e) {
                    e.printStackTrace();
                }


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) // 2
                    uri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID.concat(".provider"), file);
                else
                    uri = Uri.fromFile(file); // 3
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri); // 4
                startActivityForResult(pictureIntent, CAMERA_ACTION_PICK_REQUEST_CODE);
            }
        });*/

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }
        });



      /*  activityResultLauncher=registerForActivityResult(new ActivityResultContracts.StartActivityForResult()
                , new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        Bundle extras=result.getData().getExtras();
                        Uri imageUri;
                        Bitmap imageBitmap=(Bitmap) extras.get("data");


                        *//*WeakReference<Bitmap> res=new WeakReference<>(Bitmap.createScaledBitmap(imageBitmap,
                                imageBitmap.getHeight(),imageBitmap.getWidth()
                        ,false).copy(Bitmap.Config.RGB_565,true));

                        Bitmap bm=res.get();*//*

                        imageUri=saveImage(imageBitmap,MainActivity.this);
                        //openCropActivity(imageUri,imageUri);
                        imageView.setImageURI(imageUri);
                        textView.setText(""+imageUri);

                    }
                });*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

           /* if(data.getData()==null){
                imageBitmap = (Bitmap)data.getExtras().get("data");
            }*/
           Uri uri = data.getData();
            Uri imageUri;
            //CropData();
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageUri=saveImage(imageBitmap,MainActivity.this);
            //Uri parseUri = Uri.parse(""+imageUri);
            openCropActivity(imageUri);
          /*  imageView.setImageURI(imageUri);
            textView.setText(""+imageUri);*/

        }/*else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){

            final Uri urii = UCrop.getOutput(data);
            imageView.setImageURI(urii);
            textView.setText(""+urii);
        }*/
    }

    private void openCropActivity(Uri imageUri) {

    }

   /* private void showImage(Uri uri) {
        File file = FileUtils.getFile(context, uri);
        InputStream inputStream = new FileInputStream(file);
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        imageView.setImageBitmap(bitmap);
    }*/

    /*private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        UCrop.of(sourceUri, destinationUri)
                .withMaxResultSize(200,200)
                .start(MainActivity.this);*/
       // return destinationUri;



   /* private File getImageFile() throws IOException {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";

        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        File file = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentPhotoPath = "file:" + file.getAbsolutePath();
        return file;

    }*/

 /*   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_ACTION_PICK_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri uri = Uri.parse(currentPhotoPath);
            openCropActivity(uri, uri);
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK) {
            Uri uri = UCrop.getOutput(data);
           *//* try {
                showImage(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }*//*
        }*/
    private Uri saveImage(Bitmap bitm, Context context) {

        File imageFolder = new File(context.getCacheDir(), "images");

        Uri uri = null;
        try {
            imageFolder.mkdirs();
            File file = new File(imageFolder,"capture.jpg");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            uri = FileProvider.getUriForFile(context.getApplicationContext(), "com.example.cropcameraimage" + ".provider", file);


        } catch (FileNotFoundException e) {


        } catch (IOException e) {
            e.printStackTrace();
        }
    return uri;
    }
}


   /* private void showImage(Uri imageUri) throws FileNotFoundException {
        File file = FileUtils.g
        InputStream inputStream = new FileInputStream(file);
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        imageView.setImageBitmap(bitmap);
    }*/

    /*private void openCropActivity(Uri sourceUri, Uri destinationUri) {
        UCrop.of(sourceUri, destinationUri)
                .withMaxResultSize(200, 200)
                .withAspectRatio(5f, 5f)
                .start(MainActivity.this);

    }
}*/
