package com.example.tdccamera;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;



/*import com.canhub.cropper.CropImage;
import com.canhub.cropper.CropImageView;*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    ImageView image;
    Button button;
    TextView textview;
Cropkotlin c = new Cropkotlin();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image=findViewById(R.id.imageView);
        button=findViewById(R.id.btnView);
        textview=findViewById(R.id.textView);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c.startCrop();
            }
        });
    }



 /*   @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                image.setImageURI(resultUri);
                *//*try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    resultUri=saveImage(bitmap,MainActivity.this);
                } catch (IOException e) {
                    e.printStackTrace();
                }*//*
                *//*File finalFile = new File(getRealPathFromURI(MainActivity.this,resultUri));
                image.setScaleType(ImageView.ScaleType.FIT_XY);
                image.setImageURI(resultUri);*//*

                *//*InputStream imageStream;
                try {
                    imageStream = getContentResolver().openInputStream(resultUri);
                    Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    image.setImageBitmap(selectedImage);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }*//*
               // File finalFile = new File(getRealPathFromURI(resultUri));

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }*/

    private Uri saveImage(Bitmap bitmap, Context context) {
        File imageFolder = new File(context.getCacheDir(), "images");

        Uri uri = null;
        try {
            imageFolder.mkdirs();
            File file = new File(imageFolder,"capture.jpg");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            uri = FileProvider.getUriForFile(context.getApplicationContext(), "com.example.tdccamera" + ".provider", file);


        } catch (FileNotFoundException e) {


        } catch (IOException e) {
            e.printStackTrace();
        }
        return uri;
    }

    private String getRealPathFromURI(Context context, Uri resultUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(resultUri, proj, null, null, null);
        int column_index = 0;
        String result = "";
        if (cursor != null) {
            column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();
            return result;
        }
        return result;
    }



}