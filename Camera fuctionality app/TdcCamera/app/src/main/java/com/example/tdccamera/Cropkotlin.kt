package com.example.tdccamera

import androidx.appcompat.app.AppCompatActivity
import com.canhub.cropper.CropImageContract
import com.canhub.cropper.CropImageView
import com.canhub.cropper.options


 class Cropkotlin : AppCompatActivity() {

    fun startCrop() {

        cropImage.launch(
            options {
                setImageSource(includeGallery = false,includeCamera = true)
                setGuidelines(CropImageView.Guidelines.ON)

            }
        )
    }
    private val cropImage = registerForActivityResult(CropImageContract()) { result ->
        if (result.isSuccessful) {
            // use the returned uri
            val uriContent = result.uriContent
        } else {
            // an error occurred
            val exception = result.error
        }
    }

}