package com.example.newcrop;

import static java.security.AccessController.getContext;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    Button button;
    TextView textView;
    static final int ACTION_COMPARE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView=findViewById(R.id.image);
        textView=findViewById(R.id.textView);
        button=findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,ACTION_COMPARE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTION_COMPARE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            Uri imageUri;
            //CropData();
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageUri=saveImage(imageBitmap,MainActivity.this);

            Crop(imageUri);


           /* CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();*/
                //imageView.setImageURI(resultUri);
            }
            //imageView.setImageBitmap(imageBitmap);
        }

    private void Crop(Uri imageUri) {
        Intent intent = CropImage.activity(imageUri)
                .getIntent(MainActivity.this);
        startActivityForResult(intent, ACTION_COMPARE);
    }

    private Uri saveImage(Bitmap bitm, Context context) {
        File imageFolder = new File(context.getCacheDir(), "images");

        Uri uri = null;
        try {
            imageFolder.mkdirs();
            File file = new File(imageFolder,"capture.jpg");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitm.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            uri = FileProvider.getUriForFile(context.getApplicationContext(), "com.example.newcrop" + ".provider", file);


        } catch (FileNotFoundException e) {


        } catch (IOException e) {
            e.printStackTrace();
        }
        return uri;
    }

}
