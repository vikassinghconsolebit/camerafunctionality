package com.example.cam;

import static android.content.ContentValues.TAG;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.ByteArrayOutputStream;
import java.io.File;

public class MainActivity extends Activity {
    ImageView image;
    Button button;
    final int PIC_CROP = 2;
    private Uri picUri;
    static final int REQUEST_IMAGE_CAPTURE = 69;
    //Private static final int CAMERA_REQUEST = 1888;
    final int CROP_PIC_REQUEST_CODE = 1;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            init();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,REQUEST_IMAGE_CAPTURE);
                //button.setVisibility(View.GONE);
            }
        });
    }

    private void init() {
        button=findViewById(R.id.btn);
        image=findViewById(R.id.image);
        textView=findViewById(R.id.textView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            picUri=data.getData();

            //CropData();
           // Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");


            Uri tempUri = getImageUri(getApplicationContext(), imageBitmap);

            File finalFile = new File(getRealPathFromURI(tempUri));
            //image.setImageBitmap(imageBitmap);
            //Uri selectedImage = Uri.fromFile(new File("/storage/emulated/0/Pictures/Title (11).jpg"));
            /*Uri selectedImage=Uri.fromFile(finalFile);
            Log.i(TAG, Uri.fromFile(finalFile).toString());
            openCropActivity(selectedImage, selectedImage);*/

        }/*else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            final Uri u = UCrop.getOutput(data);
            image.setImageURI(u);
            textView.setText(""+u);
        }*/

    }


    /*private void cropImage(Uri final) {

        Intent cropIntent = new Intent("com.android.camera.action.CROP");

        cropIntent.setDataAndType(final, "image/*");
        cropIntent.putExtra("crop", "true");
        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);
        cropIntent.putExtra("outputX", 128);
        cropIntent.putExtra("outputY", 128);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, REQUEST_IMAGE_CROP);
    }*/

    private String getRealPathFromURI(Uri tempUri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(tempUri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

 /*   private Uri getImageUri(Context applicationContext, Bitmap imageBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(applicationContext.getContentResolver(), imageBitmap, "Title", null);
        return Uri.parse(path);
    }*/
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        Bitmap OutImage = Bitmap.createScaledBitmap(
                inImage, 1000, 1000,true);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), OutImage, "Title", null);
        return Uri.parse(path);
    }
}